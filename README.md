www.grafixprint.it
====================
Main website of Grafixprint company.

Developers
-----------
- Claudio Maradonna (claudio@unitoo.pw)

Designers
---------
- Stefano Amandonico (info@grafixprint.it)

License
-------
Licensed under AGPLv3 (see COPYING).

All images not listed below and logo have "all rights reserved".

`callout.jpg` licensed under CC0 license.
